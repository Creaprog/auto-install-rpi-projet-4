#!/bin/bash

#Script d'installation Owncloud !
#A exécuter en root.

echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/community:/nightly/Debian_7.0/ /' >> /etc/apt/sources.list.d/owncloud.list 
apt-get update -y
apt-get upgrade -y

apt-get install apache2 -y
apt-get install  php5 -y
apt-get install mysql-server
apt-get install phpmyadmin -y

apt-get install owncloud

apt-get install fail2ban -y
apt-get install htop -y 
apt-get install iftop -y

